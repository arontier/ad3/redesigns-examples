# arontier/ad3/documents/redesigns-examples

This is a repository for redesigns app example files.

## Prerequisites

You should have 

* `<api-key-string>`: API key of your AD3 REST API client machine
* `<token-string>`: a user's auth token

to consume AD3 REST API endpoints. And
you should do the following demonstration on your AD3 REST API client machine.

## How to use

### Getting examples

First git-clone this repository and change directory into it:

```bash
git clone https://gitlab.com/arontier/coworkers/cimplrx/redesigns-examples
cd redesigns-examples/examples
```

### Submitting work
To submit a work, run:

```bash
curl --location --request POST 'https://rest.ad3.io/api/v1/redesigns/works/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>' \
  --form 'description=testing-redesigns' \
  --form 'receptor=@input/4BIC_B_protein.pdb' \
  --form 'ligand=@input/4BIC_B_IE4.pdb'
```

If our AD3 REST API server is not heavily loaded, it will finish this example in about 65 minutes.

### Listing works

To list all submitted works, run:

```bash
curl --location --request GET 'https://rest.ad3.io/api/v1/redesigns/works/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>'
```

### Getting work back

To see specific work, run:

```bash
curl --location --request GET 'https://rest.ad3.io/api/v1/redesigns/works/<work-id>/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>'
```

where

* `<work-id>` is the value of `id` returned in the response of "listing works" API request.

A download link `outputs_link` for your submitted work is in the response of the above request.
Download the result using this link and compare with the one in `output` directory.

## References

* REST API Documentation: [https://docs.ad3.io/#/redesigns](https://docs.ad3.io/#/redesigns)

You may need a read access privilege to see the following references:

* Source repository: [https://github.com/arontier/libarontier](https://github.com/arontier/libarontier)
* Docker, workflow repository: [https://gitlab.com/arontier/ad3/redesigns](https://gitlab.com/arontier/ad3/redesigns)
